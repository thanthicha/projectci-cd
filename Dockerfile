# base image
FROM node:16-alpine

# create & set working directory
WORKDIR /app

# Install missing shared libraries
RUN apk add --no-cache libc6-compat

# install dependencies
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

# copy source files
COPY . .

# copy env.prod to .env files
RUN cp .env-prod .env

RUN yarn build

EXPOSE 3000

ENTRYPOINT ["yarn"]

CMD ["start"]
