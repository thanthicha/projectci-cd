import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { connectDb } from 'src/utils/mongodb';
import bcrypt from 'bcryptjs';

export default NextAuth({
  session: {
    jwt: true
  },
  callbacks: {
    session: async ({ session, token, user }) => {
      if (!session) return;

      const { db } = await connectDb();
      const result = await db.collection('users')
        .findOne({
          email: session.user.email
        });

      return {
        ...session,
        user: {
          name: result.name,
          username: result.username,
          email: result.email
        }
      };
    }
  },
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      async authorize (credentials, req) {
        const { db } = await connectDb();

        const user = await db.collection('users')
          .findOne({
            username: credentials.username
          });

        if (!user) {
          throw new Error('Username or Password is incorrect');
        }

        const checkPassword = await bcrypt.compare(credentials.password, user.password);
        if (!checkPassword) {
          throw new Error('Username or Password is incorrect');
        }
        //Else send success response
        return {
          name: user.name,
          username: user.username,
          email: user.email
        };
      }
    })
  ]
});
