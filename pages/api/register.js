import bcrypt from 'bcryptjs';
import { connectDb } from 'src/utils/mongodb';

export default async function ApiRegister(req, res) {
  const { db } = await connectDb();

  const {
    body,
    method
  } = req;

  if (req.method === 'POST') {
    const {
      fullName,
      email,
      username,
      password,
      passwordConfirmation,
      role
    } = body;
    if (!fullName || !username || !email || !email.includes('@') || !password || password !== passwordConfirmation) {
      res.status(422).json({ message: 'Invalid Data' });
      return;
    }

    const isUserExist = await db
      .collection('users')
      .findOne({
        $or: [{
          username: username
        }, {
          email: email
        }]
      });

    if (isUserExist) {
      res.status(422).json({ message: 'User already exists' });
      return;
    }

    const status = await db
      .collection('users')
      .insertOne({
        name: fullName,
        email,
        username,
        role,
        password: await bcrypt.hash(password, 12)
      });
    res.status(201).json({ message: 'User created', ...status });
  } else {
    res.setHeader('Allow', ['POST']);
    res.status(405).end(`Method ${method} Not Allowed`);
  }
};
