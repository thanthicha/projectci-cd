import { connectDb } from 'src/utils/mongodb';
import { ObjectId } from 'mongodb';

export default async function ApiUserDetail(req, res) {
  const { db } = await connectDb();

  const {
    body,
    query,
    method
  } = req;

  if (req.method === 'PUT') {
    const status = await db
      .collection('users')
      .updateOne({
        _id: ObjectId(query.id)
      }, {
        $set: {
          name: body.fullName,
          role: body.role
        }
      });
    res.status(200).json({ message: 'User Updated', ...status });
  } else if (req.method === 'DELETE') {
    const status = await db
      .collection('users')
      .deleteOne({
        _id: ObjectId(query.id)
      });
    res.status(200).json({ message: 'User Deleted', ...status });
  } else {
    res.setHeader('Allow', ['PUT', 'DELETE']);
    res.status(405).end(`Method ${method} Not Allowed`);
  }
};
