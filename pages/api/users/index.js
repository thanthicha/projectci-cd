import { connectDb } from 'src/utils/mongodb';

export default async function ApiUserList(req, res) {
  const { db } = await connectDb();

  const users = await db
    .collection('users')
    .find({})
    .toArray();
  res.json(users);
};
