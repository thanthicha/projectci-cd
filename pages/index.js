import Head from 'next/head';
import Layout from 'src/components/layouts';
import Login from 'src/components/login';
import { useState } from 'react';
import Register from 'src/components/register';
import { CssBaseline, Grid } from '@mui/material';

export default function PageHome () {
  const [actionState, setActionState] = useState('register');

  return (
    <Layout>
      <Grid container component='main' sx={{ height: '100vh' }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: 'url(https://source.unsplash.com/random)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: (t) => t.palette.grey[50],
            backgroundSize: 'cover',
            backgroundPosition: 'center'
          }}
        />
        {actionState === 'login'
          ? <Login onChangeForm={setActionState} />
          : <Register onChangeForm={setActionState} />
        }
      </Grid>
    </Layout>
  );
}
