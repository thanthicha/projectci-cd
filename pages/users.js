import Layout from 'src/components/layouts';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { signOut, useSession } from 'next-auth/react';
import axios from 'axios';
import {
  AppBar,
  Backdrop,
  Box,
  Button,
  CircularProgress, Container, IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
  Toolbar,
  Typography
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import ModalEditUser from 'src/components/modal-edit-user';
import Swal from 'sweetalert2';

export default function PageUser () {
  const router = useRouter();
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);

  const { data: session, status } = useSession({
    required: true,
    onUnauthenticated: () => router.push('/')
  });

  const fetchUsers = () => {
    axios.get('/api/users').then(({ data }) => {
      setUsers(data);
    });
  };

  const onSignOut = async () => {
    await signOut({ redirect: false });
    await router.push('/');
  };

  const onDeleteUser = (user) => {
    Swal.fire({
      title: 'Are you sure!',
      text: 'Do you want to continue delete a user?',
      icon: 'warning',
      showDenyButton: true,
      confirmButtonText: 'Yes delete it',
      denyButtonText: `Don't delete it`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        axios.delete(`/api/users/${user._id}`).then(resp => {
          Swal.fire({
            title: resp.data.message,
            icon: 'success',
            didClose: () => fetchUsers()
          });
        }).catch((e) => {
          Swal.fire({
            title: e.response.data.message,
            icon: 'error'
          });
        });
      }
    });
  };

  const onOpenEditUserModal = (user) => {
    setSelectedUser(user);
    setIsEditModalOpen(true);
  };

  const onCloseEditUserModal = () => {
    setIsEditModalOpen(false);
    setSelectedUser(null);
  };

  const onCloseEditUserModalWithFetch = () => {
    onCloseEditUserModal();
    fetchUsers();
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  console.log(session);
  return (
    <Layout>
      {status === 'loading'
        ? <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open
        >
          <CircularProgress color='inherit' />
        </Backdrop>
        : <Box>
          <AppBar position='static'>
            <Toolbar>
              <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
                Manage User
              </Typography>
              <Typography component='div' sx={{ mr: 1 }}>
                Hi, {session.user.name}
              </Typography>
              <Button color='inherit' onClick={onSignOut}>Logout</Button>
            </Toolbar>
          </AppBar>
          <Container sx={{ my: 3 }}>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label='simple table'>
                <TableHead>
                  <TableRow>
                    <TableCell>Full Name</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Username</TableCell>
                    <TableCell>Role</TableCell>
                    <TableCell align='right'>Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {users.map((user) => (
                    <TableRow
                      key={user.username}
                      sx={{
                        '&:nth-of-type(odd)': {
                          backgroundColor: (t) => t.palette.action.hover
                        },
                        '&:last-child td, &:last-child th': {
                          border: 0
                        }
                      }}
                    >
                      <TableCell>{user.name}</TableCell>
                      <TableCell>{user.email}</TableCell>
                      <TableCell>{user.username}</TableCell>
                      <TableCell>{user.role}</TableCell>
                      <TableCell align='right'>
                        <IconButton color='primary' onClick={() => onOpenEditUserModal(user)}>
                          <EditIcon />
                        </IconButton>
                        {user.username !== session.user.username &&
                          <IconButton color='error' onClick={() => onDeleteUser(user)}>
                            <DeleteIcon />
                          </IconButton>}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Container>
        </Box>
      }
      {isEditModalOpen && <ModalEditUser
        user={selectedUser}
        handleClose={onCloseEditUserModal}
        handleCloseWithFetch={onCloseEditUserModalWithFetch} />}
    </Layout>
  )
    ;
}
