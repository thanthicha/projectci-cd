import {
  Avatar,
  Box,
  Button,
  Grid,
  Link,
  TextField,
  Typography
} from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { useForm, Controller } from 'react-hook-form';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';

export default function Login ({ onChangeForm }) {
  const router = useRouter();
  const { control, handleSubmit } = useForm();

  const onSubmit = async (data) => {
    console.log(data);
    const status = await signIn('credentials', {
      redirect: false,
      username: data.username,
      password: data.password,
    });
    if (status.error) {
      Swal.fire({
        title: status.error,
        icon: 'error'
      });
    } else {
      Swal.fire({
        title: 'Login successful',
        icon: 'success',
        didClose: () => router.push('/users')
      });
    }
    console.log(status);
  }

  return (
    <Grid item xs={12} sm={8} md={5} elevation={6} square="true">
      <Box
        sx={{
          my: 8,
          mx: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign in
        </Typography>
        <Box component='form' noValidate onSubmit={handleSubmit(onSubmit)} sx={{ mt: 1 }}>
          <Controller
            control={control}
            name='username'
            rules={{ required: true }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  label='Username'
                  placeholder='Username...'
                  margin='normal'
                  variant='standard'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={!!fieldState.error}
                  fullWidth
                />
              );
            }}
          />
          <Controller
            control={control}
            name='password'
            rules={{ required: true }}
            render={({ field, fieldState }) => {
              console.log(fieldState.error);
              return (
                <TextField
                  {...field}
                  type='password'
                  label='Password'
                  placeholder='Password...'
                  margin='normal'
                  variant='standard'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={!!fieldState.error}
                  fullWidth
                />
              );
            }}
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Box sx={{ textAlign: 'center' }}>
            <Link href='#' variant='body2' onClick={(e) => {
              e.preventDefault();
              onChangeForm('register');
            }}>
              {'Don\'t have an account? Sign Up'}
            </Link>
          </Box>
        </Box>
      </Box>
    </Grid>
  );
}
