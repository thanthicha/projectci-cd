import * as React from 'react';
import { Controller, useForm } from 'react-hook-form';
import axios from 'axios';
import Swal from 'sweetalert2';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl, FormHelperText,
  InputLabel, MenuItem, Select,
  TextField
} from '@mui/material';

export default function ModalEditUser ({ user, handleClose, handleCloseWithFetch }) {
  const { control, handleSubmit } = useForm({
    defaultValues: {
      fullName: user.name,
      role: user.role
    }
  });

  const onSubmit = async (data) => {
    try {
      const resp = await axios.put(`/api/users/${user._id}`, data);
      Swal.fire({
        title: resp.data.message,
        icon: 'success',
        didClose: () => handleCloseWithFetch()
      });
    } catch (e) {
      Swal.fire({
        title: e.response.data.message,
        icon: 'error'
      });
      console.log(e);
    }
  };

  return (
    <Dialog open={true} onClose={handleClose} sx={{ zIndex: 999 }}>
      <DialogTitle>Edit User: {user.name}</DialogTitle>
      <DialogContent>
        <Controller
          control={control}
          name='fullName'
          rules={{ required: true }}
          render={({ field, fieldState }) => {
            return (
              <TextField
                {...field}
                label='Full Name'
                placeholder='Name...'
                margin='normal'
                variant='standard'
                InputLabelProps={{
                  shrink: true
                }}
                error={!!fieldState.error}
                fullWidth
              />
            );
          }}
        />
        <Controller
          control={control}
          name='role'
          rules={{ required: true }}
          render={({ field, fieldState }) => {
            return (
              <FormControl
                variant="standard"
                margin='normal'
                error={!!fieldState.error}
                fullWidth
              >
                <InputLabel id="role-select-label">Role</InputLabel>
                <Select
                  {...field}
                  labelId="role-select-label"
                  id="role-select"
                  label="Role"
                >
                  <MenuItem value='System Administrator'>Systems Administrator</MenuItem>
                  <MenuItem value='Project Manager'>Project Manager</MenuItem>
                  <MenuItem value='Developer'>Developers</MenuItem>
                  <MenuItem value='Tester'>Testers</MenuItem>
                </Select>
                {fieldState.error && <FormHelperText>{fieldState.error.message}</FormHelperText>}
              </FormControl>
            );
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSubmit(onSubmit)}>Save</Button>
      </DialogActions>
    </Dialog>
  );
}
