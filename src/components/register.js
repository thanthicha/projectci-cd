import {
  Avatar,
  Box,
  Button, FormControl, FormHelperText,
  Grid, InputLabel,
  Link, MenuItem, Select,
  TextField,
  Typography
} from '@mui/material';
import AssignmentIcon from '@mui/icons-material/Assignment';
import { Controller, useForm } from 'react-hook-form';
import axios from 'axios';
import Swal from 'sweetalert2';

export default function Register ({ onChangeForm }) {
  const { control, handleSubmit, watch, reset } = useForm();

  const password = watch('password', '');

  const onSubmit = async (data) => {
    try {
      const resp = await axios.post('/api/register', data);
      reset({
        fullName: '',
        email: '',
        username: '',
        password: '',
        passwordConfirmation: '',
        role: ''
      });
      Swal.fire({
        title: resp.data.message,
        icon: 'success',
        didClose: () => onChangeForm('login')
      });
    } catch (e) {
      Swal.fire({
        title: e.response.data.message,
        icon: 'error'
      });
      console.log(e);
    }
  };

  return (
    <Grid item xs={12} sm={8} md={5} elevation={6} square='true'>
      <Box
        sx={{
          my: 8,
          mx: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <AssignmentIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign Up
        </Typography>
        <Box component='form' noValidate onSubmit={handleSubmit(onSubmit)} sx={{ mt: 1 }}>
          <Controller
            control={control}
            name='fullName'
            rules={{ required: true }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  label='Full Name'
                  placeholder='Name...'
                  margin='normal'
                  variant='standard'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={!!fieldState.error}
                  fullWidth
                />
              );
            }}
          />
          <Controller
            control={control}
            name='email'
            rules={{ required: true }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  type='email'
                  label='Email'
                  placeholder='Email address...'
                  margin='normal'
                  variant='standard'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={!!fieldState.error}
                  fullWidth
                />
              );
            }}
          />
          <Controller
            control={control}
            name='username'
            rules={{ required: true }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  label='Username'
                  placeholder='Username...'
                  margin='normal'
                  variant='standard'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={!!fieldState.error}
                  fullWidth
                />
              );
            }}
          />
          <Controller
            control={control}
            name='password'
            rules={{ required: true }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  type='password'
                  label='Password'
                  placeholder='Password...'
                  margin='normal'
                  variant='standard'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={!!fieldState.error}
                  fullWidth
                />
              );
            }}
          />
          <Controller
            control={control}
            name='passwordConfirmation'
            rules={{
              required: true,
              validate: value => value === password || 'The passwords confirmation does not match'
            }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  type='password'
                  label='Repeat Password'
                  placeholder='Repeat Password...'
                  margin='normal'
                  variant='standard'
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={!!fieldState.error}
                  helperText={fieldState.error && fieldState.error.message}
                  fullWidth
                />
              );
            }}
          />
          <Controller
            control={control}
            name='role'
            rules={{ required: true }}
            render={({ field, fieldState }) => {
              return (
                <FormControl
                  variant="standard"
                  margin='normal'
                  error={!!fieldState.error}
                  fullWidth
                >
                  <InputLabel id="role-select-label">Role</InputLabel>
                  <Select
                    {...field}
                    labelId="role-select-label"
                    id="role-select"
                    label="Role"
                  >
                    <MenuItem value='System Administrator'>System Administrator</MenuItem>
                    <MenuItem value='Project Manager'>Project Manager</MenuItem>
                    <MenuItem value='Developer'>Developer</MenuItem>
                    <MenuItem value='Tester'>Tester</MenuItem>
                  </Select>
                  {fieldState.error && <FormHelperText>{fieldState.error.message}</FormHelperText>}
                </FormControl>
              );
            }}
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 3, mb: 2 }}
          >
            Register
          </Button>
          <Box sx={{ textAlign: 'center' }}>
            <Link href='#' variant='body2' onClick={(e) => {
              e.preventDefault();
              onChangeForm('login');
            }}>
              {'Already have an account? Sign In'}
            </Link>
          </Box>
        </Box>
      </Box>
    </Grid>
  );
}
