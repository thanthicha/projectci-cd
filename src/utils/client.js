import axios from 'axios';
import { UPSTASH_ENDPOINT, UPSTASH_TOKEN } from 'src/constants';

export const apiClient = axios.create({
  baseURL: UPSTASH_ENDPOINT,
  headers: {
    'Authorization': `Bearer ${UPSTASH_TOKEN}`
  }
});
